#!/bin/bash

# remove vim configuration, plugins etc.
rm -rf ~/.vim*

# add .vimrc
cp .vimrc ~/.vimrc

# install pathogen.vim
mkdir -p ~/.vim/autoload ~/.vim/bundle
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# get vim plugins
repo_list=(
    "altercation/vim-colors-solarized.git"
    "tpope/vim-vividchalk.git"
    "vim-syntastic/syntastic.git"
    "rodjek/vim-puppet.git"
    "pearofducks/ansible-vim.git"
    "sjl/gundo.vim.git"
    "tpope/vim-surround"
    "mileszs/ack.vim.git"
    );

for repo in "${repo_list[@]}"
do
   plugin_name=$(echo "${repo}" | cut -d '/' -f 2 | cut -d '.' -f 1)
   git clone https://github.com/"${repo}" ~/.vim/bundle/"${plugin_name}"
done
