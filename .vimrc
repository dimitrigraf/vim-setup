execute pathogen#infect()
filetype plugin indent on

syntax enable
set background=dark
"let g:solarized_termcolors=256 "activate if terminal colorscheme != solarized

"colorscheme solarized
colorscheme vividchalk

set tabstop=4       " number of visual spaces per tab
set softtabstop=4   " number of spaces in tab when editing
set shiftwidth=4
set expandtab       " tabs are spaces

" custom identing for yaml files
autocmd FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab

set number          " show line numbers
set showcmd         " show command in bottom bar
set cursorline      " highlight current line

" Add full file path to your statusline
set statusline+=%F
set laststatus=2    " actually make the statusline visible

" color group trailing whitespaces
:highlight ExtraWhitespace ctermbg=red guibg=red
" show trailing whitespace at the end
:match ExtraWhitespace /\s\+\%#\@<!$/

set wildmenu        " visual autocomplete for command menu
set lazyredraw      " redraw only when we need to.

set showmatch       " highlight matching braces [{()}]

set incsearch       " search while typing
set hlsearch        " highlight matches

let mapleader=","   " leader is comma

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" jk is escape
inoremap jk <esc>

" edit vimrc and load vimrc bindings
nnoremap <leader>ev :vsp $MYVIMRC<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>

" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>

" replace word under cursor, confirmation for each occurence
nnoremap <Leader>r :%s/\<<C-r><C-w>\>//gc<Left><Left><Left>
" globally replace word under cursor
nnoremap <Leader>R :%s/\<<C-r><C-w>\>//g<Left><Left>

" prepare search with ack.vim
nnoremap <Leader>s :Ack!<Space>
